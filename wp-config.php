<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'growyourcash' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', 'root' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Hi;M:F-_fT=ZnWxtDS2aLDkL=E)~n|c!fj?r!j[&h*EZ0+:~9eDGbfPO$ak8e4kr' );
define( 'SECURE_AUTH_KEY',  'blb85^uS$szI,R{yi[qb6bk0+~U*7H*&_-0N)cr>G{TBou)+-r*5iC$<hn9(0[7o' );
define( 'LOGGED_IN_KEY',    '[A}}D[r*RVB8Y(07-EU%mM5IV4^8efy@D?.J|PG4n;DVeSdHPLJ98*>(y4@]hGE$' );
define( 'NONCE_KEY',        '`mPDTjy~d3@]jHME)hS-l29sTa7PT:r|L[I^MZ62=,Uk;sTKcpIkcD%^R<wa=ZRN' );
define( 'AUTH_SALT',        'k-rT {}UnkM(5%HU;QdZIOsDar4||IYc;,L%5HE2,Y*57aj=@DpM I:no&UIPm{h' );
define( 'SECURE_AUTH_SALT', '0V2Xx?mABJGTeMU+o:CXF?{5[4lrQ:5g>L0vWb!;}lZ_X{~2/2[peITxmq[ P0sw' );
define( 'LOGGED_IN_SALT',   'rmXo]rJtEw({G_7kaT}*Q(&~U_AIyA@u{YwiSSyk?K;XQg %w|u]BPVMW3|(z4V(' );
define( 'NONCE_SALT',       '09W-W[>bBvM*%^nWK.nN=J`9O][XO~z9h0%GWYurSULL2A8P2c81C2^zh?Ob?|Q=' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'nono_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
